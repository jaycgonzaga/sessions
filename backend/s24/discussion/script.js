// [SECTION] While Loop
let count = 5;

// while (count !== 0) {
// 	console.log("Current value of count: " + count);
// 	count--;
// }


// [SECTION] Do-while loop
// let number = Number(prompt("Give me a number:"));

// do {
// 	number += 1;

// 	console.log("Current value of number: " + number);

// } while (number < 10)


// [SECTION] For loop
// for(let count = 0; count <= 20; count++){
// 	console.log("Current for loop value: " + count);
// }


let my_string = "earl";

// To get the length of a string
console.log(my_string.length);

// To get a specific letter in a string
console.log(my_string[0]);

// Loops through each letter in the string and will keep iterating as long as the current index is less than the length of the string.
for(let index = 0; index < my_string.length; index++){
	console.log(my_string[index]); //starts at letter 'e' and will keep printing each letter up to 'l'.
}

// MINI ACTIVITY (20 mins.) Finish by 9:40AM
// 1. Loop through the 'my_name' variable which has a string with your name on it.
// 2. Display each letter in the console but EXCLUDE all the vowels from it.
// 3. Send a screenshot of the output in our B303 google hangouts
let my_name = "Earl Rafael Diaz"

for(let index = 0; index < my_name.length; index++){
	// For filtering the vowels
	if(
		my_name[index].toLowerCase() == "a" ||
		my_name[index].toLowerCase() == "e" ||
		my_name[index].toLowerCase() == "i" ||
		my_name[index].toLowerCase() == "o" ||
		my_name[index].toLowerCase() == "u" 
	){
		// If the current letter matches any of the vowels, it will just print an empty string
		// console.log("");
		continue; // If we use 'continue' keyword, it will skip the else block and reiterate the loop to check if the next letter is a vowel.
	} else {
		// Prints the current letter if it is not a vowel
		console.log(my_name[index]);
	}
}


let name_two = "rafael";

for(let index = 0; index < name_two.length; index++){

	if(name_two[index].toLowerCase() == "a"){
		console.log("Skipping...");
		continue; // Reiterates the loop if the current letter is 'a'
	} else if(name_two[index].toLowerCase() == "e"){
		break; // Breaks/stops the loop if the current letter is 'e'
	} else {
		console.log(name_two[index]); // Prints the letter if it is not 'a' or 'e'
	}

}