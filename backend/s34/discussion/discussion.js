// CRUD Operations
/*
	- CRUD means Create, Retrieve, Update, and Delete.
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// [Section] Inserting documents (Create)

// Insert one document
/*
    - Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
    - The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
    - Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.
    - By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.
    - Syntax
        - db.collectionName.insertOne({object});
    - JavaScript syntax comparison
        - object.object.method({object});
*/
db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "0912345678",
        email: "janedoe@gmail.com"
    },
    courses: [ "CSS", "Javascript", "Python" ],
    department: "none"
});

// Inserting multiple documents at once
db.users.insertMany([
    { 
        "firstName": "John",
        "lastName": "Doe"
    },
    { 
        "firstName": "Joseph",
        "lastName": "Doe"
    }
]);

// [SECTION] Retrieving documents
// Retrieving all the inserted users
db.users.find();

// Retrieving a specific document from a collection
db.users.find({ "firstName": "John" });


// [SECTION] Updating existing documents
// For updating a single document
db.users.updateOne(
    // First part - retrieves a specific user using the ID.
    {
        "_id": ObjectId("64c1c45f98a5baa752634263") 
    },
    // Second part - uses the $set keyword to set a specific property of that user to a new value.
    {
        $set: {
            "lastName": "Gaza"
        }
    }
); 

// For updating multiple documents
// updateMany() allows for modification of 2 or more documents as compared to updateOne() which can only accomodate a single document.
db.users.updateMany(
    {
        "lastName": "Doe"
    },
    {
        $set: {
            "firstName": "Mary"
        }
    }
);


// [SECTION] Deleting documents from a collection
// Deleting multiple documents
db.users.deleteMany({ "lastName": "Doe" });

// Deleting single document
db.users.deleteOne({
    "_id": ObjectId("64c1c45f98a5baa752634263")
});