console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

fetch('https://jsonplaceholder.typicode.com/posts')
    .then(response => response.json())
    .then(posts => console.log(posts));

async function fetchData(){
    let result =await fetch('https://jsonplaceholder.typicode.com/posts')

    let json_result =await result.json();

    console.log(json_result);
}

fetchData();

fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    headers:{
        'Content-Type' : 'application/json'
    },
    body: JSON.stringify({
        title: "New post!",
        body: "Hello World.",
        userId: 2
    })
})
.then(response => response.json())
.then(created_post => console.log(created_post));

fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PUT',
    headers:{
        'Content-Type' : 'application/json'
    },
    body: JSON.stringify({
        title: "Updated Post!"
    })
})
.then(response => response.json())
.then(created_post => console.log(created_post));

fetch('https://jsonplaceholder.typicode.com/posts/userId=1')
.then(response => response.json())
.then(post => console.log(post));

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then(response => response.json())
.then(post => console.log(comments));