let http = require('http');

const port = 4000;

const app = http.createServer((req, res) => {
    if(req.url == '/greeting'){
        res.writeHead(200, {'Content-Type': 'text/plain'})

        res.end('Hello Again');
    } else if(req.url == '/homepage'){
        res.writeHead(200, {'Content-Type': 'text/plain'})

        res.end('Welcome to the homepage.');
    } else {
        res.writeHead(404, {'Content-Type': 'text/plain'})

        res.end('Error 404: Page not found hahahaha');
    }
})

app.listen(port);

console.log(`Server now accessible at localhost:${port}. `);
