// use 'require' directive to load the Node.js modules
// a module is a software component or part of a program that contains one or more routines
// the "http" module lets Node.js transfer data using the HYPER TEXT TRANSFER PROTOCOL
// HTTP is a protocol that allows the fetching of resources like HTML Documents
let http = require("http");

// createServer() method creates an HTTP server that listens to requests on a specific port and gives response back to the client
// The http module has a createServer() method that accepts a function as an argument and allows for a creation of a server
// The arguments passed in the function are request and response objects (data type) that contains methods that allow us to receive requests from the client and send responses back to it
// 4000 is the port where the server will listen to. A port is a virtual point where the netork connections start and end
http.createServer(function(request, response) {

	// 200 - status code for the response - means OK
	// sets the content-type of the response to ba a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});

	// send the response with text content 'Hello World'
	response.end('Hello World');

}).listen(4000)

// When server is running, console will print the message:
console.log('Server is running at port 4000');